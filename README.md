# POFImageGallery

SOLUTION:
Apart from all of requirements mentioned in email, following are the achievements/considerations

✔       Separate Network, Caching, Model and Controller layers to make project more scaleable.
✔       Separate datasource from controller to avoid massive Controller
✔       Generic logics are separated in form of categories to make code more resuable.
✔       Dependency injections to make code less coupled and make it easier to test via injecting mocks.
✔       Image caching is done via NSCache for runtime caching. For Disc Cache, used NSCacheDirectory Storage.
✔       Network layer optimised to handle less threads and objects simultaneously thus download cancellation mechanism is introduced
✔       Physical groups are grouped to depict shape of real big project
✔       Test Cases  are introduced along with mocks to test lower layers only.
✔       UI if optimised for all devices in all orientations.
✔       Real Device (iPhone 7) performance of 60 fps with average runtime memory foot print of   50MB±5.

IMPROVEMENTS:

✔      Current test coverage is just to demonstrate the ability and thus adding some More Test Coverage can have real benefits
✔      Alternative optimisation approaches like offscreen rendering and adding shadows, borders etc directly inside image using core graphics can significantly increase performance.
✔       If UI structure in controller is complex MVVM pattern can be used to further break it down. 


//
//  POFAPIClient.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "POFAPIClient.h"

static NSString* END_POINT = @"http://jsonplaceholder.typicode.com/photos";

@interface POFAPIClient(){}
@property(nonatomic, strong) id<POFImageStore> cache;
@property(atomic, strong) NSMutableDictionary<NSString*, NSURLSessionDataTask*> *tasksInProgress;
@property (nonatomic, strong) NSURLSession *session;
@end

@implementation POFAPIClient

-(instancetype)initWith:(id<POFImageStore>)cache session:(NSURLSession *)session {
    self = [super init];
    if (self) {
        self.cache = cache;
        self.session = session;
        self.tasksInProgress = [NSMutableDictionary new];
    }
    return self;
}

-(void)fetchPhotos: (PhotosCompletion) completion{
    NSURL *photosURL = [NSURL URLWithString:END_POINT];
    NSURLSessionDataTask *imageDownloadTask = [self taskForURL:photosURL completion:^(NSData *data, NSError *error) {
        if (error) {
            completion(nil, error);
            return;
        }
        //Parse here the data to JSON
        NSError *jsonError;
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        
        if (jsonError) {
            completion(nil, jsonError);
            return;
        }
        
        if ([jsonResponse isKindOfClass:[NSArray class]]) {
            NSArray *photosArray = (NSArray*) jsonResponse;
            NSMutableArray<POFImage*> *images = [NSMutableArray new];
            for (NSDictionary* photo in photosArray) {
                id obj = [[POFImage alloc] initWithDictionary:photo];
                [images addObject:obj];
            }
            completion(images, nil);
        }
    }];
    
    [imageDownloadTask resume];
}

-(void)cancelDownloadImageForURL: (NSURL*) imageURL {
    NSString *taskKey = imageURL.absoluteString;
    __weak POFAPIClient *weakSelf = self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        NSURLSessionDataTask *task = [self.tasksInProgress valueForKey: taskKey];
        if (!task) {
            return;
        }
        
        [task cancel];
        
        [weakSelf.tasksInProgress removeObjectForKey: taskKey];
    });
}

-(void)downloadImage:(NSURL *)imageURL completion:(ImageCompletion)completion{
    
    // If present return image from cache
    UIImage *existingImage = [self.cache imageForKey:imageURL.absoluteString];
    if (existingImage) {
        completion(existingImage);
        return;
    }
    
    __weak id<POFImageStore> weakCache = self.cache;
    __weak NSMutableDictionary *weakTasksInProgress = self.tasksInProgress;
    NSString *taskKey = imageURL.absoluteString;
    
    NSURLSessionDataTask *imageDownloadTask = [self taskForURL:imageURL completion:^(NSData *data, NSError *error) {
        
        //Remove from the tasks list
        [weakTasksInProgress removeObjectForKey: taskKey];
        if (error) {
            completion(nil);
            return;
        }
        
        UIImage *image = [UIImage imageWithData:data];
        // save image to cache
        [weakCache setImage:image forKey:imageURL.absoluteString];
        completion(image);
    }];
    
    [self.tasksInProgress setObject:imageDownloadTask forKey:taskKey];
    
    [imageDownloadTask resume];
}

-(NSURLSessionDataTask*)taskForURL: (NSURL*) url completion: (void (^)(NSData *data, NSError *error)) completion {
    return [self.session dataTaskWithURL:url
                  completionHandler:
            ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        //Response parse
        if (error != nil) {
            completion(nil,error);
            return;
        }
        
        //Filter for success status codes
        //range 200..299
        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
            NSRange successRange = NSMakeRange(200, 99);
            BOOL success = NSLocationInRange(httpResponse.statusCode,
                                             successRange);
            if(!success) {
                //TODO: Create NSError here and return...!!!
                completion(nil, nil);
                return;
            }
        }
        
        completion(data, nil);
    }];
}

@end

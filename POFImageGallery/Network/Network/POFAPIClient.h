//
//  POFAPIClient.h
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "POFImage.h"
#import "POFImageStore.h"
#import <UIKit/UIKit.h>

typedef void (^PhotosCompletion) (NSArray<POFImage *> *, NSError*);
typedef void (^ImageCompletion) (UIImage*);

@interface POFAPIClient : NSObject
-(instancetype)initWith:(id<POFImageStore>)cache session: (NSURLSession*) session;
-(void)fetchPhotos: (PhotosCompletion) completion;
-(void)downloadImage: (NSURL*) imageURL completion: (ImageCompletion) completion;
-(void)cancelDownloadImageForURL: (NSURL*) imageURL;
@end

//
//  POFImageStore.h
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol POFImageStore
-(void)setImage:(UIImage*)image forKey:(NSString*)key;
-(UIImage*)imageForKey:(NSString*)key;
-(void)deleteImageForKey:(NSString*)key;
@end

@interface POFImageStoreImpl : NSObject <POFImageStore>

@end

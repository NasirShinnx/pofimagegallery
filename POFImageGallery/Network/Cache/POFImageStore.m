//
//  POFImageStore.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POFImageStore.h"
#import "NSString+Escape.h"

@interface POFImageStoreImpl() {

}
@property(nonatomic, strong)NSCache *cache;
-(NSURL*)imageURLforKey:(NSString*)key;

@end

@implementation POFImageStoreImpl

- (instancetype) init
{
    self = [super init];
    if (self) {
        self.cache = [NSCache new];
        self.cache.countLimit = 20;
    }
    return self;
}

-(NSURL*)imageURLforKey:(NSString*)key {
    NSArray *cacheDirectories = [[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *cahceDirectory = [cacheDirectories firstObject];
    return [cahceDirectory URLByAppendingPathComponent:key];
    
}

-(void)setImage:(UIImage*)image forKey:(NSString*)key {
    NSString* escapedKey = [key escapeForwardSlash];
    [self.cache setObject:image forKey:escapedKey];
    NSURL *imageURL = [self imageURLforKey:escapedKey];
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToURL:imageURL atomically:true];
}

-(UIImage*)imageForKey:(NSString*) key {
    NSString* escapedKey = [key escapeForwardSlash];
    UIImage *existingImage = (UIImage*)[self.cache objectForKey:escapedKey];
    if (existingImage) {
        return  existingImage;
    }
    
    NSURL *imageURL = [self imageURLforKey:escapedKey];
    UIImage *imageFromDisk = [UIImage imageWithContentsOfFile:imageURL.absoluteString];
    if (imageFromDisk == nil) {
        return nil;
    }

    [self.cache setObject:imageFromDisk forKey:escapedKey];
    return  imageFromDisk;
}

-(void)deleteImageForKey:(NSString*) key {
    NSError *error;
    NSString* escapedKey = [key escapeForwardSlash];
    [self.cache removeObjectForKey:escapedKey];
    NSURL *imageURL = [self imageURLforKey:escapedKey];
    [[NSFileManager defaultManager] removeItemAtURL:imageURL error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
}

@end

//
//  UIView+UIView_Shadow.h
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UIView (Shadow)

-(void)addShadowWith:(UIColor*)color andWidth:(NSInteger) width;
-(void)addBorderWith:(UIColor*)color andWidth:(NSInteger) width;
-(void)addCurveWithRadius:(CGFloat) radius;

@end

//
//  NSMutableArray+Shuffle.h
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSMutableArray (Filter)

- (NSIndexSet *)filterOnCustomCriteria:(NSPredicate*)predicate;
@end

//
//  NSMutableArray+Shuffle.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "NSMutableArray+Randomize.h"

@implementation NSMutableArray (Filter)

- (NSIndexSet *)filterOnCustomCriteria:(NSPredicate*)predicate {
    NSIndexSet *indexes = [self indexesOfObjectsPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [predicate evaluateWithObject:obj];
    }];
    return indexes;
}

@end

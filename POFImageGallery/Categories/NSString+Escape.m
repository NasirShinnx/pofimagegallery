//
//  NSMutableArray+Shuffle.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "NSString+Escape.h"

@implementation NSString (Escape)

- (NSString*)escapeForwardSlash {
    return [self stringByReplacingOccurrencesOfString:@"/" withString:@""];
}


@end

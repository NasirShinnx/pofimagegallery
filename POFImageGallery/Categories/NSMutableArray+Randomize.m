//
//  NSMutableArray+Shuffle.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "NSMutableArray+Randomize.h"

@implementation NSMutableArray (Randomize)

- (NSMutableArray*)randomize {
    return [NSMutableArray recursiveRandomize:[self mutableCopy] count:self.count];
}

+(NSMutableArray*) recursiveRandomize:(NSMutableArray *) array count:(NSInteger)count {
    if (count <= 1) {
        return array;
    }
    // generate a random number between 0 and n-2
    NSInteger min = 0;
    NSInteger max = count-2;
    NSInteger index = min + arc4random_uniform((uint32_t)(max-min+1));
    [array exchangeObjectAtIndex:count-1 withObjectAtIndex:index];
    return [NSMutableArray recursiveRandomize:array count:count-1];
}

@end

//
//  UIView+UIView_Shadow.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "UIView+Shadow.h"

@implementation UIView (Shadow)

-(void)addShadowWith:(UIColor*)color andWidth:(NSInteger) width {
    self.layer.shadowRadius  = 1.5f;
    self.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    self.layer.shadowOffset  = CGSizeMake(2.0f, 2.0f);
    self.layer.shadowOpacity = 0.9f;
    self.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.bounds, shadowInsets)];
    self.layer.shouldRasterize = true;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.layer.shadowPath    = shadowPath.CGPath;
    
 
}

-(void)addBorderWith:(UIColor*)color andWidth:(NSInteger) width {
    self.layer.borderWidth = width;
    self.layer.borderColor = color.CGColor;
}

-(void)addCurveWithRadius:(CGFloat) radius{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = true;
}

@end

//
//  ViewController.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "POFGalleryViewController.h"
#import "POFAPIClient.h"
#import "POFGalleryDataSource.h"
#import "NSMutableArray+Randomize.h"
#import "NSMutableArray+Filter.h"

@interface POFGalleryViewController () <UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong) POFGalleryDataSource *dataSource;
@property(nonatomic, strong) POFAPIClient *apiClient;


@end

@implementation POFGalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.apiClient = [[POFAPIClient alloc] initWith:[POFImageStoreImpl new] session:[NSURLSession sharedSession]];
    
    //Instantiate data source
    self.dataSource = [[POFGalleryDataSource alloc] initWith:self.apiClient];
    
    __weak POFGalleryViewController *weakSelf = self;
    [self.apiClient fetchPhotos:^(NSArray<POFImage *> * photos, NSError *error) {
        if (photos != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.dataSource.photos = photos;
                [weakSelf.collectionView reloadData];
            });
        }
        else{
            [weakSelf displayError:error];
        }
    }];
    
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delegate = self;
}

- (IBAction)randomizedPressed:(id)sender {
    id mutatingPhotos = self.dataSource.photos.mutableCopy;
    self.dataSource.photos = [[mutatingPhotos randomize] copy];
    [self.collectionView reloadData];
}

- (IBAction)removePressed:(id)sender {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.title CONTAINS[cd] %@ OR SELF.title CONTAINS[cd] %@", @"b",@"d"];
    NSIndexSet *indexSetToRemove = [self.dataSource.photos.mutableCopy filterOnCustomCriteria:predicate];

    NSMutableArray *indexPathToRemove = [NSMutableArray new];
    [indexSetToRemove enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
        [indexPathToRemove addObject:[NSIndexPath indexPathForItem:idx inSection:0]];
    }];

    NSMutableArray *array = [NSMutableArray arrayWithArray:self.dataSource.photos];
    [array removeObjectsAtIndexes:indexSetToRemove];

    __weak POFGalleryViewController *weakSelf = self;
    [self.collectionView performBatchUpdates:^{
        self.dataSource.photos = array;
        [self.collectionView deleteItemsAtIndexPaths:indexPathToRemove];
    } completion:^(BOOL finished) {
        weakSelf.removeButton.enabled = false;
    }];
    
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    POFImage *image = self.dataSource.photos[indexPath.item];
    [self.apiClient cancelDownloadImageForURL:image.url];
}

-(void) displayError: (NSError*) error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

@end

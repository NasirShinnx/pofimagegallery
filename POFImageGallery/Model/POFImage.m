//
//  POFImage.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "POFImage.h"

static NSString *const kPOFImage_Title = @"title";
static NSString *const kPOFImage_ThumbnailUrl = @"thumbnailUrl";
static NSString *const kPOFImage_Url = @"url";
static NSString *const kPOFImage_ImageId = @"id";

@implementation POFImage

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        self.title = [dictionary valueForKey:kPOFImage_Title];
        NSString *thumbnailURLStr = [dictionary valueForKey:kPOFImage_ThumbnailUrl];
        self.thumbnailUrl = [NSURL URLWithString:thumbnailURLStr];
        NSString *imageURStr = [dictionary valueForKey:kPOFImage_Url];
        self.url = [NSURL URLWithString:imageURStr];
        self.imageId = [dictionary valueForKey:kPOFImage_ImageId];
    }
    return self;
}
@end

//
//  POFImage.h
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface POFImage : NSObject {
}

-(instancetype)initWithDictionary:(NSDictionary*) dictionary;

@property (nonatomic, strong) NSNumber *albumId;
@property (nonatomic, strong) NSNumber *imageId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSURL *thumbnailUrl;
@end

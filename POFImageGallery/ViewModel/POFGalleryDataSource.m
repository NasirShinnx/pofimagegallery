//
//  POFGalleryDataSource.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "POFGalleryDataSource.h"
#import "POFPhotoCollectionViewCell.h"
#import "POFImage.h"

@interface POFGalleryDataSource()
@property(nonatomic, strong) POFAPIClient *apiClient;
@end
@implementation POFGalleryDataSource

- (instancetype)initWith:(POFAPIClient*) client
{
    self = [super init];
    if (self) {
        self.photos = [NSArray new];
        self.apiClient = client;
    }
    return self;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    POFPhotoCollectionViewCell *cell =  [collectionView dequeueReusableCellWithReuseIdentifier:@"POFPhotoCollectionViewCell" forIndexPath:indexPath];
    POFImage *cellModel = [self.photos objectAtIndex:indexPath.item];
    cell.labelTitle.text = cellModel.title;
    cell.tag = cellModel.imageId.integerValue;
    
    __weak POFImage *weakCellModel = cellModel;
    __weak POFPhotoCollectionViewCell *weakCell = cell;
    
        [self.apiClient downloadImage:cellModel.url completion:^(UIImage *image) {
            dispatch_async(dispatch_get_main_queue(), ^{
            
                if(image && weakCell.tag == weakCellModel.imageId.integerValue) {
                    weakCell.imageView.image = image;
                }
            });
        }];
    
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.photos count];
}

@end

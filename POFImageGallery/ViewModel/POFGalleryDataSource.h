//
//  POFGalleryDataSource.h
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "POFAPIClient.h"
@class POFImage;
@interface POFGalleryDataSource : NSObject<UICollectionViewDataSource>

- (instancetype)initWith:(POFAPIClient*) client;

@property (nonatomic, strong) NSArray<POFImage*> *photos;

@end

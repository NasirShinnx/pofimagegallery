//
//  PhotoCollectionViewCell.m
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import "POFPhotoCollectionViewCell.h"
#import "UIView+Shadow.h"

@interface POFPhotoCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) UIImage *placeholder;
@end

@implementation POFPhotoCollectionViewCell

-(void)awakeFromNib {
    [super awakeFromNib];
    [self.bgView addShadowWith:[UIColor redColor] andWidth:10.0];
    [self.imageView addBorderWith:[UIColor redColor] andWidth:5.0];
    [self.imageView addCurveWithRadius:10.0];
    self.labelTitle.transform = CGAffineTransformMakeRotation(M_PI / 4);
    self.placeholder = [UIImage imageNamed:@"placeholder"];
    self.imageView.image = self.placeholder;
}

-(void)prepareForReuse {
    self.imageView.image = self.placeholder;
}

@end

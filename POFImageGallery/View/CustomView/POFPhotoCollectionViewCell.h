//
//  PhotoCollectionViewCell.h
//  POFImageGallery
//
//  Created by Nasir Mahmood on 09.06.18.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POFImage.h"

@interface POFPhotoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) NSNumber *imageId;

@end

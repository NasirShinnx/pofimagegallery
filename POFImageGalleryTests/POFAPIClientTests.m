//
//  POFAPIClientTests.m
//  POFImageGalleryTests
//
//  Created by Nasir on 10/06/2018.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>
#import "POFAPIClient.h"
#import "POFImageStore.h"

@interface MockCache : NSObject  <POFImageStore> {}
@property (strong) NSString *lastFetchedImageKey;
@property (strong) NSString *lastSetImageKey;
@end

@interface MockURLSession: NSURLSession {}
@property (strong) NSURL *lastFetchedURL;
@end

@interface MockURLDataTask: NSURLSessionDataTask
@end

@interface POFAPIClientTests : XCTestCase
@property (strong) POFAPIClient *client;
@property (strong) MockCache *cache;
@property (strong) MockURLSession *session;
@end

@implementation POFAPIClientTests

- (void)setUp {
    [super setUp];
    self.cache = [MockCache new];
    self.session = [[MockURLSession alloc] init];
    self.client = [[POFAPIClient alloc] initWith:self.cache
                                         session:self.session];
}

- (void)testFetchPhotos {
    //GIVEN
    NSURL *mockURL = [NSURL URLWithString:@"http://www.google.com"];
    
    //WHEN
    [self.client downloadImage:mockURL completion:^(UIImage *name) {}];
    
    //THEN
    XCTAssertEqual(mockURL.absoluteString, self.session.lastFetchedURL.absoluteString);
}

@end

@implementation MockCache

- (void)deleteImageForKey:(NSString *)key {}

- (UIImage *)imageForKey:(NSString *)key {
    self.lastFetchedImageKey = key;
    return nil;
}

- (void)setImage:(UIImage *)image forKey:(NSString *)key {
    self.lastSetImageKey = key;
}
@end

@implementation MockURLSession
- (NSURLSessionDataTask *)dataTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error))completionHandler {
    self.lastFetchedURL = url;
    return [[MockURLDataTask alloc] init];
}
@end

@implementation MockURLDataTask

-(void)cancel {
    
}

-(void)resume {
    //Don't need to call network
}

@end

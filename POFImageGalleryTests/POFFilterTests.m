//
//  POFFilterTests.m
//  POFImageGalleryTests
//
//  Created by Nasir on 10/06/2018.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSMutableArray+Filter.h"
#import "POFImage.h"
@interface POFFilterTests : XCTestCase

@end

@implementation POFFilterTests

- (void)setUp {
    [super setUp];
}

- (void)testFilter {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.title CONTAINS[cd] %@ OR SELF.title CONTAINS[cd] %@", @"b",@"d"];
    
    POFImage* pofImage0 = [[POFImage alloc] initWithDictionary:@{@"title":@"fg hi jk"}];
    
    POFImage* pofImage1 = [[POFImage alloc] initWithDictionary:@{@"title":@"ab ac ad"}];
    
    POFImage* pofImage2 = [[POFImage alloc] initWithDictionary:@{@"title":@"lm no pq"}];
    
    NSMutableArray *array = [NSMutableArray arrayWithObjects:pofImage0,pofImage1,pofImage2, nil];
    
    NSIndexSet* indexes  = [array filterOnCustomCriteria:predicate];
    
    XCTAssertNotNil(indexes);
    
    XCTAssertEqual(indexes.firstIndex, 1);
    
}


@end

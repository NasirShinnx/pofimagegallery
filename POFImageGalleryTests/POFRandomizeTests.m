//
//  POFRandomizeTests.m
//  POFImageGalleryTests
//
//  Created by Nasir on 10/06/2018.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSMutableArray+Randomize.h"
@interface POFRandomizeTests : XCTestCase

@end

@implementation POFRandomizeTests

- (void)setUp {
    [super setUp];
}

-(void) testRandomizeWith0ObjectsArray {
    NSMutableArray *array = [NSMutableArray new];
    
    NSMutableArray* randomizedArray  = [array randomize];
    
    XCTAssertNotNil(randomizedArray);
}

-(void) testRandomizeWith1ObjectArray {
    NSMutableArray *array = [NSMutableArray arrayWithObjects:@"1", nil];
    
    NSMutableArray* randomizedArray  = [array randomize];
    
    XCTAssertNotNil(randomizedArray);
    
    XCTAssertEqual(randomizedArray.count, array.count);
}

-(void) testRandomizeWithXObjectArray {
    NSMutableArray *array = [NSMutableArray arrayWithObjects:@"1",@"2", nil];
    
    NSMutableArray* randomizedArray  = [array randomize];
    
    XCTAssertNotNil(randomizedArray);
    
    XCTAssertEqual(randomizedArray.count, array.count);
    
    XCTAssertEqual(array[0], randomizedArray[1]);
    
    XCTAssertEqual(array[1], randomizedArray[0]);


}


@end

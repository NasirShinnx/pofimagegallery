//
//  POFImageStoreTests.m
//  POFImageGalleryTests
//
//  Created by Nasir on 10/06/2018.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "POFImageStore.h"
@interface POFImageStoreTests : XCTestCase
@property(strong) POFImageStoreImpl *imageStore;
@property(strong) UIImage *testAbleImage;
@end

@implementation POFImageStoreTests

- (void)setUp {
    [super setUp];
    
    self.imageStore = [POFImageStoreImpl new];
    
    self.testAbleImage = [UIImage new];
}

-(void) testImageSave {
    [self.imageStore setImage:self.testAbleImage forKey:@"TestKey"];
    
    UIImage *retrievedImage = [self.imageStore imageForKey:@"TestKey"];
    
    XCTAssertEqual(self.testAbleImage, retrievedImage);
}

-(void) testDeleteImage {
    [self.imageStore setImage:self.testAbleImage forKey:@"TestKey"];
    
    [self.imageStore deleteImageForKey:@"TestKey"];
    
    UIImage* retrievedImage = [self.imageStore imageForKey:@"TestKey"];
    
    XCTAssertNil(retrievedImage);
}



@end

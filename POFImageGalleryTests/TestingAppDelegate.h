//
//  TestingAppDelegate.h
//  POFImageGalleryTests
//
//  Created by Nasir on 10/06/2018.
//  Copyright © 2018 Nasir Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestingAppDelegate : UIResponder <UIApplicationDelegate>

@end
